#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "matrix.h"
#include "io.h"
#include "game.h"
#include "board.h"

/*
* Samuel Schultze - SI - 09/04/2018 - Lab II
* gcc -Wall *.c
*/

int main() {
	srand((unsigned)time(NULL));

	do {
		StartNewGame();
	} while(toupper(readChar("\nJogar novamente (S/N)? ")) == 'S');
}