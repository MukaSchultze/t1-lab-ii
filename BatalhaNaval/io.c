#include "io.h"

int readInt(char* label) {
	int result;
	printf("%s", label);
	scanf("%d", &result);
	return result;
}

char readChar(char* label) {
	char result;
	printf("%s", label);
	scanf(" %c", &result);
	return result;
}

void readPosition(char* label, int* x, char* y) {
	printf("%s", label);
	scanf(" %c%d", y, x);
}