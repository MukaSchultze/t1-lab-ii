#pragma once
#include <stdlib.h>
#include <stdbool.h>

typedef struct boardSpace {
	int boatSize;
	bool hit;
	bool vertical;

	struct boardSpace* prev;
	struct boardSpace* next;

} BoardSpace;

typedef struct {

	int width;
	int height;
	BoardSpace** m;

} Board;

#include "matrix.h" //C � mto estranho cara WTF!!!

Board GenerateBoard(int width, int height);
BoardSpace GetDefaultEmptyBoardSpace();
int RandomRange(int min, int max);
void RandomizeBoard(Board* b, int count5, int count4, int count3, int count2);
bool CanPlaceBoat(Board* b, int x, int y, bool vertical, int size);
void PlaceBoat(Board *b, int x, int y, bool vertical, int size);
void PlaceBoatRandom(Board* b, int size);
void PrintBoards(Board* b0, Board* b1);
char GetCharForBoardSpace(BoardSpace space);
bool BoatFullyHit(BoardSpace space);
bool BoardCompleted(Board* b);