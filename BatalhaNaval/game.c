#include <stdio.h>
#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include "io.h"
#include "timing.h"
#include "game.h"

void StartNewGame() {
	int width = readInt("Largura: ");
	while(width > 20 || width < 5)
		width = readInt("Largura deve estar entre 5 e 20\nLargura: ");

	int height = readInt("Altura: ");
	while(height > 20 || height < 5)
		height = readInt("Altura deve estar entre 5 e 20\nAltura: ");

	int count5 = readInt("Quantidade de barcos de 5 casas: ");
	int count4 = readInt("Quantidade de barcos de 4 casas: ");
	int count3 = readInt("Quantidade de barcos de 3 casas: ");
	int count2 = readInt("Quantidade de barcos de 2 casas: ");

	char mp = toupper(readChar("Multiplayer (S/N)? "));
	while(mp != 'S' && mp != 'N')
		mp = toupper(readChar("Responda com S ou N\nMultiplayer (S/N)? "));

	mp = mp == 'S';

	int player = 1;
	clock_t startTime = GetClock();
	Board b0 = GenerateBoard(width, height);
	Board b1 = GenerateBoard(width, height);

	RandomizeBoard(&b0, count5, count4, count3, count2);
	RandomizeBoard(&b1, count5, count4, count3, count2);

	while(true) {
		Board b = player == 1 ? b0 : b1;

		for(int i = 0; i < 50; i++)
			printf("\n");

		printf("\n\nVez do jogador %d\n", player);
		PrintElapsedTime(startTime);
		PrintBoards(&b0, &b1);

		if(player == 2 && !mp)
			PlayRoundAI(&b);
		else
			PlayRound(&b);

		if(BoardCompleted(&b))
			break;

		if(player == 1)
			player++;
		else
			player--;
	}

	freeMatrix(b0.m, b0.width, b0.height);
	freeMatrix(b1.m, b1.width, b1.height);
	printf("\nFim do jogo!\nO Jogador %d venceu!!!\n", player);
}

void PlayRound(Board* b) {
	int x = 0;
	int y = 0;

	readPosition("Posi��o: ", &y, (char*)&x);

	x = toupper(x) - 'A';
	y--;

	//printf("%d\n", x);
	//printf("%d\n", y);

	if(x < 0 || y < 0 || x >= b->width || y >= b->height) {
		printf("\nPosi��o inv�lida, a posi��o deve seguir a ordem A1 - Z9 e estar dentro dos limites do tabuleiro\n");
		PlayRound(b);
		return;
	}

	if(b->m[x][y].hit) {
		printf("\nA posi��o %c%d j� foi atingida\n", y, x);
		PlayRound(b);
		return;
	}

	b->m[x][y].hit = true;
}

bool AIPlayAt(Board* b, int x, int y, int* lastHitX, int* lastHitY) {
	b->m[x][y].hit = true;

	if(b->m[x][y].boatSize) {
		*lastHitX = x;
		*lastHitY = y;
	}

	return b->m[x][y].boatSize;
}

void PlayRoundAI(Board* b) {

	int x = RandomRange(0, b->width);
	int y = RandomRange(0, b->height);

	while(b->m[x][y].hit) {
		x = RandomRange(0, b->width);
		y = RandomRange(0, b->height);
	}

	static int lastHitY = -1;
	static int lastHitX = -1;

	if(lastHitX != -1 && lastHitY != -1) {
		BoardSpace* lastHit = &b->m[lastHitX][lastHitY];
		BoardSpace* first;
		BoardSpace* last;

		for(last = lastHit; last->next != NULL; last = last->next);
		for(first = lastHit; first->prev != NULL; first = first->prev);

		x = lastHitX;
		y = lastHitY;

		if(lastHit->vertical)
			if(lastHit == last)
				y -= lastHit->boatSize - 1;
			else
				y++;
		else
			if(lastHit == last)
				x -= lastHit->boatSize - 1;
			else
				x++;
	}

	AIPlayAt(b, x, y, &lastHitX, &lastHitY);

	if(lastHitX != -1 && lastHitY != -1 && BoatFullyHit(b->m[lastHitX][lastHitY])) {
		lastHitX = -1;
		lastHitY = -1;
	}
}