#pragma once
#include <string.h>
#include "io.h"
#include "matrix.h"
#include "board.h"

void StartNewGame();
void PlayRound(Board* b);
void PlayRoundAI(Board* b);