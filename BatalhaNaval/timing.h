#pragma once

clock_t GetClock();
void PrintElapsedTime(clock_t start);