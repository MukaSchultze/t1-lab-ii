#include "board.h"

Board GenerateBoard(int width, int height) {
	Board b;

	b.width = width;
	b.height = height;
	b.m = allocMatrixWithValue(width, height, GetDefaultEmptyBoardSpace());

	return b;
}

BoardSpace GetDefaultEmptyBoardSpace() {
	BoardSpace space;

	space.boatSize = 0;
	space.hit = false;
	space.vertical = false;
	space.next = NULL;
	space.prev = NULL;

	return space;
}

int RandomRange(int min, int max) {
	return min + (rand() % (max - min));
}

void RandomizeBoard(Board* b, int count5, int count4, int count3, int count2) {
	for(int i = 0; i < count5; i++)
		PlaceBoatRandom(b, 5);
	for(int i = 0; i < count4; i++)
		PlaceBoatRandom(b, 4);
	for(int i = 0; i < count3; i++)
		PlaceBoatRandom(b, 3);
	for(int i = 0; i < count2; i++)
		PlaceBoatRandom(b, 2);
}

bool IsPosAvailable(Board* b, int x, int y) {
	if(x < 0 || y < 0)
		return false;
	if(x >= b->width || y >= b->height)
		return false;

	return b->m[x][y].boatSize == 0;
}

bool CanPlaceBoat(Board* b, int x, int y, bool vertical, int size) {
	if(vertical) {
		for(int i = y; i < y + size; i++) {
			if(!IsPosAvailable(b, x, i))
				return false;
			if(x > 0 && !IsPosAvailable(b, x - 1, i))
				return false;
			if(x + 1 < b->width && !IsPosAvailable(b, x + 1, i))
				return false;
		}

		if(y + size < b->height && !IsPosAvailable(b, x, y + size))
			return false;

		if(y > 0 && !IsPosAvailable(b, x, y - 1))
			return false;
	}
	else {
		for(int i = x; i < x + size; i++) {
			if(!IsPosAvailable(b, i, y))
				return false;
			if(y > 0 && !IsPosAvailable(b, i, y - 1))
				return false;
			if(y + 1 < b->height && !IsPosAvailable(b, i, y + 1))
				return false;
		}

		if(x + size < b->width && !IsPosAvailable(b, x + size, y))
			return false;

		if(x > 0 && !IsPosAvailable(b, x - 1, y))
			return false;
	}

	return true;
}

void PlaceBoat(Board* b, int x, int y, bool vertical, int size) {
	for(int i = 0; i < size; i++)
		if(vertical) {
			b->m[x][y + i].boatSize = size;
			b->m[x][y + i].vertical = vertical;

			if(i < size - 1)
				b->m[x][y + i].next = &b->m[x][y + i + 1];
			if(i > 0)
				b->m[x][y + i].prev = &b->m[x][y + i - 1];
		}
		else {
			b->m[x + i][y].boatSize = size;
			b->m[x + i][y].vertical = vertical;

			if(i < size - 1)
				b->m[x + i][y].next = &b->m[x + i + 1][y];
			if(i > 0)
				b->m[x + i][y].prev = &b->m[x + i - 1][y];
		}
}

void PlaceBoatRandom(Board* b, int size) {
	int attemps = 0;
	int randX = 0;
	int randY = 0;
	bool vertical = 0;

	do {
		randX = RandomRange(0, b->width);
		randY = RandomRange(0, b->height);
		vertical = RandomRange(0, 2);

		if(attemps++ > 100000) {
			printf("\nEscolhe menos barcos :(\n");
			return;
		}
	} while(!CanPlaceBoat(b, randX, randY, vertical, size));

	PlaceBoat(b, randX, randY, vertical, size);
}

void PrintBoards(Board* b0, Board* b1) {
	printf("\n     ");

	for(int x = 0; x < b0->width; x++)
		printf("%c ", 'A' + x);

	printf("        ");

	for(int x = 0; x < b0->width; x++)
		printf("%c ", 'A' + x);

	printf("\n   -");

	for(int x = 0; x <= b0->width; x++)
		printf("--");

	printf("     -");

	for(int x = 0; x <= b0->width; x++)
		printf("--");

	printf("\n");

	for(int y = 0; y < b0->height; y++) {
		printf("%02d | ", y + 1);

		for(int x = 0; x < b0->width; x++)
			printf("%c ", GetCharForBoardSpace(b0->m[x][y]));

		printf("|  %02d | ", y + 1);

		for(int x = 0; x < b0->width; x++)
			printf("%c ", GetCharForBoardSpace(b1->m[x][y]));

		printf("|\n");
	}

	printf("   -");

	for(int x = 0; x <= b0->width; x++)
		printf("--");

	printf("     -");

	for(int x = 0; x <= b0->width; x++)
		printf("--");

	printf("\n");
}

char GetCharForBoardSpace(BoardSpace space) {
	if(!space.hit)
		return '~';
	else if(!space.boatSize)
		return '#';
	else	if(BoatFullyHit(space))
		return 'X';
	else
		return '0' + space.boatSize;
}

bool BoatFullyHit(BoardSpace space) {
	if(!space.hit)
		return false;

	for(BoardSpace* next = space.next; next != NULL; next = next->next)
		if(!next->hit)
			return false;

	for(BoardSpace* prev = space.prev; prev != NULL; prev = prev->prev)
		if(!prev->hit)
			return false;

	return true;
}

bool BoardCompleted(Board* b) {
	for(int x = 0; x < b->width; x++)
		for(int y = 0; y < b->height; y++)
			if(b->m[x][y].boatSize && !BoatFullyHit(b->m[x][y]))
				return false;

	return true;
}