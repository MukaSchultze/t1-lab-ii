#include <time.h>
#include <stdio.h>
#include "timing.h"

clock_t GetClock() {
	return clock();
}

void PrintElapsedTime(clock_t start) {
	int secs = (clock() - start) / CLOCKS_PER_SEC;
	int mins = secs / 60;

	secs %= 60;
	printf("Passaram-se %02d minutos e %02d segundos\n", mins, secs);
}