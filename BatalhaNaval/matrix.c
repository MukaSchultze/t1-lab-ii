#include "matrix.h"

MATRIX_TYPE** allocMatrix(int width, int height) {
	MATRIX_TYPE** m = malloc(width * sizeof(MATRIX_TYPE*));

	for(int i = 0; i < width; i++)
		m[i] = malloc(height * sizeof(MATRIX_TYPE));

	return m;
}

MATRIX_TYPE** allocMatrixWithValue(int width, int height, MATRIX_TYPE defaultValue) {
	MATRIX_TYPE** m = allocMatrix(width, height);

	for(int x = 0; x < width; x++)
		for(int y = 0; y < height; y++)
			m[x][y] = defaultValue;

	return m;
}

void freeMatrix(MATRIX_TYPE** m, int width, int height) {
	for(int x = 0; x < width; x++)
		free(m[x]);

	free(m);
}