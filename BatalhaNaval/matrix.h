#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "board.h"
#define MATRIX_TYPE BoardSpace

MATRIX_TYPE** allocMatrix(int width, int height);
MATRIX_TYPE** allocMatrixWithValue(int width, int height, MATRIX_TYPE defaultValue);

void freeMatrix(MATRIX_TYPE** m, int width, int height);