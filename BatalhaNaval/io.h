#pragma once
#include <stdio.h>

int readInt(char* label);
char readChar(char* label);
void readPosition(char* label, int* x, char* y);